
## Roadmap for Q1 - January to March 2023

### Sponsor 9

* Work on improvements coming out of usability research

### Sponsor 96

* UX have usability testing running from February–Match for desktop, so expect amends.
* Webtunnel PT happening in Q1

### Sponsor 30

* Conjure PT integration in TB

### Sponsor 131

* Release private browser in March
  * base browser + theming/branding,
  * extensions
  * installers/packages
* Modernize testsuites for TB:
  * evaluating the test landscape, determining the tests that make sense and spending time on working on improving in this area.
  * Privacy Browser needs tests too
  * get ./mach tests working
* rlbox sandbox (the MR is ready for review already :))
* switch all the Android projects to the right SDK version, and avoid another 11.5.9
  * or, even better, avoid another crash storm like 11.5.8!
* fix the crashes with assertions on
  * last time we tried, Tor Browser was not runnable in debug mode because of some race with preferences
* start prototyping arti in android on Q2
* sustainable release process
* ESR migration starting on time.


## Roadmap for Q3 - July to September 2022

We keep the [board for the team](https://gitlab.torproject.org/groups/tpo/applications/-/boards?label_name[]=Q3) up to date but you can look at our broad goals next.

### MUST HAVE

###### [SPONSOR 9](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&label_name[]=Sponsor%209)

- phase 5: Resolve issues that will come up from user feedback

###### [SPONSOR 30](https://gitlab.torproject.org/groups/tpo/applications/-/boards?label_name[]=Sponsor%2030)

- There is a few remaining issues blocked by UX team right now.
- Resolve issues that will come up from user feedback

###### [SPONSOR 96](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&label_name[]=Sponsor%2096)

- O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android).     

###### [SPONSOR 101](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&milestone_title=Sponsor%20101%20-%20Tor%20VPN%20Client%20for%20Android)

- Evaluate orbot and leap for components and architecture.
- VPN safety criteria

###### [SPONSOR 131](https://gitlab.torproject.org/groups/tpo/applications/-/boards?label_name[]=Sponsor%20131)

* Localization Migration
* Security Level
* New Identity
* Build
* Customization
* ESR 102 migration Q3

###### Others

- TBA maintenance
  * rebase to ESR 102
  * have one of the new devs lead the project

## Roadmap for April - June 2022

### MUST HAVE

###### [SPONSOR 9](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&label_name[]=Sponsor%209)
- Resolve issues that will come up from user feedback

###### [SPONSOR 30](https://gitlab.torproject.org/groups/tpo/applications/-/boards?label_name[]=Sponsor%2030)
- There is a few remaining issues blocked by UX team right now.

###### [SPONSOR 96](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&label_name[]=Sponsor%2096)
- O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android).     

###### [SPONSOR 101](https://gitlab.torproject.org/groups/tpo/applications/-/boards?scope=all&milestone_title=Sponsor%20101%20-%20Tor%20VPN%20Client%20for%20Android)
- Evaluate orbot and leap for components and architecture.
- VPN safety criteria


- HTTPS everywhere replacement.
- TBA maintenance

### NICE TO HAVE

- Go/Rust/Java dependency resolution: how to resolve dependencies ahead of time.
  - https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40056
  - https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31588
  - https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/33953

- Integration: Begin understanding application integration/embedding
  - Begin helping Network Team create easy-to-use Arti API
  - Document required components for Android app integration

- Unblock Tor: Begin understanding web services' Tor concerns
  - Create partnerships with friendly web services
  - Establish working group with partners

## Roadmap October - December 2021

### Review Q3 - what did we do?

- ~~TB bootstrap flow (Sponsor 30)~~
- Firefox 91esr transition for Tor Browser' - 80% complete
- UX Fixes
- Audit Components
- Switch to Mozilla91
- ~~Experiment with active resistance (Sponsor 103)~~
- V2 Deprecation UI/UX. - 50% done
- ~~About:torconnect~~
- Desktop ESR Transition. - 80% done
- Enable HTTPS-Only Mode.
- Integrate libhttps-everywhere-core (replacing https-everywhere webextension) - tor-browser#40458
- Go/Rust/Java dependency resolution: how to resolve dependencies ahead of time.
- Improve bridge selection in tor browser.
- Suggest bridges based on location (Sponsor 30)

### What worked well?

- Applications team worked well with ux and anti-censorship on s30
- New esr91 proton styling look pretty good awesome!excellent, 'pretty dope', rad :call_me:, tubular
- Generally released stable desktop versions on schedule
- Improved communication this quarter

### What could work better?

- ESR transition collaboration is somewhat tricky at times
- ESR transition is slow
- Browser releases are blocked by one person (at least I can mostly do rebases now in theory -R)
- S30 torconnect Alphas weren't as well tested as we'd hoped for, needed a reasonable amount of post-launch fixing
- ESR91 branch merged to nightly almost at the end (maybe we can merge a not final version earlier)

### ROADMAP 2021 Q4

#### MUST HAVE

deliverables (discussion on scope):

* sponsor 30: http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/15
  * O3.3.1 - Implement final design of user flows part 1 and 2.
    * proposal http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser-spec/-/blob/master/proposals/106-quickstart.txt  
      * Tor Launcher should suggest the use of bridges if Tor is dangerous in user's area https://bugs.torproject.org/11132  -   
      * Inform users in Tor Launcher of which settings are best for them based on their country http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/29590  
      * Make it easier to add a bridge in network settings https://bugs.torproject.org/14638  -   
      * Show tor's bootstrapping percentage and description on about:torconnect http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40544  
      * Smarter bootstrapping: http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/29590  
      * about:preferences#tor naming confustion: http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40568  
      * Tor Network Settings button feedback: http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40556

* sponsor 96: O3.1 - Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android).  
  * http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40560  
  * http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40645 -
  * New Moat Apis   

* sponsor 101:  http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/32  
  * nothing estimated for Q4 but it would be good for everybody to follow the research that UX/UR team is doing.

releases(<http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/wikis/Release_Schedule>)

* 2021-10-05 10.5.8
* 2021-10-08 11.0a8  
* 2021-11-02 11.0

YEC updates
  * richard, matt and duncan meet during the week
  * http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40617

* TB bootstrap flow regression bugs Firefox 91esr transition for Tor
* Hire two new software engineer Begin discussing Android component ownership with Guardian Project
* V2 Deprecation UI/UX. (error page that v2 are not supported anymore)
* Go modules - http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser-build/-/issues/28325

#### NICE TO HAVE

* [Audit Components Integrate libhttps-everywhere-core](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40458)

Browser: Browser is under control

* Android Tor Browser releases are on schedule
* Android/Linux Tor Browser Automated tests are passing
* Begin surveying the Browser ecosystem

VPN: Lay VPN project foundation
* Begin designing Tor VPN architecture

Integration: Begin understanding application integration/embedding
* Begin helping Network Team create easy-to-use Arti API
* Document required components for Android app integration

Unblock Tor: Begin understanding web services' Tor concerns
* Create partnerships with friendly web services
* Establish working group with partners

Experiment with active resistance
Enable HTTPS-Only Mode.
Go/Rust/Java dependency resolution: how to resolve dependencies ahead of time.

## Roadmap (November 2020 - June 2021)

* [Sponsor 58](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/11)
* Move nightly builds into TPO infrastructure
* Stabilize Android Rapid Release processes
* [Sponsor 30](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/trac/-/issues/31265)
* Move Linux onto Rapid Release
* Stabilize/Bug-fix Android Tor Browser features
* Watershed update
* RAPPOR
* Seeding HSTS cache
* [Sponsor 103](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/21)

\--

## Roadmap (June 2021 - September 2021)

- suggest bridges based on location ([Sponsor 30](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/trac/-/issues/31265))
- TB bootstrap flow ([Sponsor 30](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/trac/-/issues/31265))
- Firefox 91esr transition for Tor Browser'
  - [UX Fixes](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40487)
  - [Audit Components](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/android-components/-/issues/34324)
  - [Switch to Mozilla91](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/tor-browser/-/issues/40466)
- experiment with active resistance ([Sponsor 103](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/21))

### Roadmap (June 2021 - October 2021)

* V2 Deprecation UI/UX.
* About:torconnect
* Desktop ESR Transition.
* Enable HTTPS-Only Mode.
* Integrate libhttps-everywhere-core (replacing https-everywhere webextension) - tor-browser#40458
* Go/Rust/Java dependency resolution: how to resolve dependencies ahead of time.
* Improve bridge selection in tor browser.
