UNFINISHED DRAFT!                                                                                                                                                                                          

# Current situation in Debian

* If you install TorBirdy + Thunderbird you will use system-wide tor.
* If you install torbrowser-launcher you will use TBB's bundled tor, not a system-wide instance even if available.
* If you install onionshare it depends on torbrowser-launcher and on TBB to be running.

=> not very consistent, several tor configurations => confused users.

# Two solutions

* One system-wide instance

* Spin up one instance per application

See trade-offs: https://trac.torproject.org/projects/tor/wiki/org/meetings/2015SummerDevMeeting/TorProcessShare

We are leaning towards "One system-wide Tor instance" on Linux, possibly as a first step towards the "Spin up one Tor instance per application" approach. So, from now on, we'll have "One system-wide Tor instance" as a goal on Linux.

# Security

We don't want each application to have full access to the system-wide tor's control port, so we need to use onion-grater, and ship a profile for each application.

Full unfiltered Tor control protocol access is bad for security reasons. Unfiltered Tor ControlPort access allows running commands such as `GETINFO address` which reveals one's real external IP address, which is bad in case of application compromise since that is counter to what users of Tor want. With filtered Tor control protocol access, if the application is running properly contained (AppArmor, containers, VMs, out of scope in this document) there is a chance to prevent it from figuring out its own external IP address or otherwise doing damage to the system. 

# Best practices

## Linux

### For packagers

* Patch or reconfigure the application to use the system-wide Tor.
* `debian/control`: `Depends: onion-grater`
* ([ RFP: onion-grater - Whitelisting Tor Control Protocol Filter](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=859125))
* Provide a profile for onion-grater and AppArmor. (And try to upstream those.)

XXX: elaborate on these dependencies.

XXX: we don't have a solution for when AppArmor is not enabled because onion-grater does not work great without it. 

### For application developers
* Implement SOCKS5 support for outgoing traffic.
* Set a socks user name for stream isolation. (Then Tor's default option `IsolateSOCKSAuth` will kick in and provide stream isolation.)
 * Consider using multiple different socks user names. For example if you are developing a multi account chat client.
 * Connection to aaa.onion for account A could use socks user name `application-name_aaa.onion_few_random_characters`.
 * Connection to bbb.onion for account B could use socks user name `application-name_bbb.onion_few_random_characters`.
 * Generate at startup and the same `few_random_characters` as long as the application is running.
  * These will help to use a new circuit on application restart as well as to isolate multiple instances of the same application from each other.
 * XXX: These guidelines for choosing a SOCKS5 username/password differ somewhat from [what TorButton does](https://gitweb.torproject.org/torbutton.git/tree/src/components/domain-isolator.js).  It would be useful to either adopt what TorButton does, or document why these guidelines are preferable.
* If your application uses Tor control protocol commands, ideally use an existing library to use it such as [python3-stem](https://packages.debian.org/search?keywords=python3-stem).
* If your application uses incoming connections...
 * XXX Review: Bind on the system's default listener. ([Listen Port Convention](https://phabricator.whonix.org/T635))
 * Use Tor ephemeral hidden services using Tor control protocol command `add_onion`.
* Describe what your application is doing on a network level, post that to the [tor-dev mailing list](https://lists.torproject.org/pipermail/tor-dev/). You would get feedback if you are doing something that would needlessly stress the Tor network. (Examples for stressing the Tor network would be a usual torrent client that uses Tor `IsolateDestAddr`.)
* Ideally use only dependencies that are already installed from official Debian packages main sources packages.debian.org. That will make getting your application into Debian easier.
* Release your application under a FSF approved Libre Software license.
* If you are using Tor control protocol...
 * Don't send more than 1 Tor ControlPort command at once.
  * I.e. do not request `status/circuit-established status/bootstrap-phase`, instead
  * do request `status/circuit-established` followed by `status/bootstrap-phase`.
  * That makes control protocol filtering easier.
* Write your application in a memory safe language.
* Provide an AppArmor profile.
* Provide an onion-grater profile.
* Decentralized serverless protocols are to be preferred.
* Have a look at existing applications already in production such as onionshare and ricochet.
* Do not invent your own crypto, ideally use existing mature solutions.
* ~~If your application is packaged in distros: Include a system-wide (in /etc) configuration toggle for using a system-wide tor instance instead of a potentially bundled tor.~~ (Should not be needed since we advice to auto detect system-wide Tor.)
* XXX Review: Use resources via HTTPS or some secure protocol. If resources are also available in an Onion Service, use it instead. Otherwise, consider running one.

#### network configuration

1. If there are environment variables, use these and skip 2. and 3.

```
## Do not start eventual bundled Tor or Tor configuration gui.
TOR_SKIP_LAUNCH=1

TOR_SOCKS_HOST=127.0.0.1
TOR_SOCKS_PORT=9050
TOR_SOCKS_IPC_PATH=/var/run/tor/socks

TOR_CONTROL_HOST=127.0.0.1
TOR_CONTROL_PORT=9051
TOR_CONTROL_PASSWD="password"
TOR_CONTROL_IPC_PATH=/var/run/tor/control                                                                                                                                                                  
TOR_CONTROL_COOKIE_AUTH_FILE==/var/run/tor/control.authcookie
```

2. Auto detect if there is system-wide Tor and use it if so.


On Linux, prefer unix domain socket files.

* `/var/run/tor/socks`
* `/var/run/tor/control`
* `/var/run/tor/control.authcookie`

Otherwise (Windows and Linux), try to fall back auto detecting if there are default system-wide Tor listening ports reachable.

* `127.0.0.1 9050`
* `127.0.0.1 9051`

3. Auto detect if there is TBB's Tor and use it if so.

* Linux:
 * `127.0.0.1 9150`
 * `127.0.0.1 9151`

* Windows:
 * XXX: Describe how to do that. Is that even sanely possible? TBB on Windows uses `SocksPort Auto`, right?

4. If your application is:

* Linux packaged: stop here
* Linux not packaged: consider starting your bundled Tor instance
* Windows: consider starting your bundled Tor instance

# What about configuration sharing?

XXX: specify how this should work. Probably by writing the settings to some file all applications will look at first.

## Linux

When we allow two cases, i.e. system-wide tor, and bundled tor, we need a way for all applications to still use the same tor configuration to connect to the Tor network (proxy, pluggable transport).

## Windows

All apps run their own Tor but should share the tor configuration for connecting to the Tor network.

# XXX Review: General Security/Anonymity Best Practices

Following the previous sections will make the application properly use Tor, but that does not make it completely secure/anonymous. It should be made an analysis of the app to identify points in its design and implementation which might harm users. While making these checks, it is also a good idea to take into account the dependencies used.

Examples of things to avoid:

* Uses some protocol which by default adds the system's/user's information to its packets.
* Stores/Transmits identifying information while it could be anonymized.                                                                                                                                   
* Some of its Internet use is unintentional because it is not known one of the dependencies makes such access.

It is also important to take security into account and not only anonymity: it should made an analysis focused on vulnerabilities that can affect the app and the system.

XXX: It would be nice if there were references on "Making Secure/Anonymous Applications" or something like that.

XXX: There is some useful additional information in the [Protocol leaks](https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO#Protocolleaks) article.

Moved from: https://gitlab.torproject.org/legacy/trac/-/wikis/doc/Tor_friendly_applications_best_practices