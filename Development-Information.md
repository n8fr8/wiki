[Codebase Overview](Development-Information/Codebases)

## Tor Browser for Android
- [Building Guide](https://gitlab.torproject.org/tpo/applications/team/-/wikis/Development-Information/Firefox-Android/Building)

## Tor Browser for Desktop
- [Tor Browser Project Organisation](Development-Information/Tor-Browser/Tor-Browser-Repository-Overview)
- [Local builds in Linux and macOS](Development-Information/Tor-Browser/dev-Build)
- [Cross-compiled builds for Windows](Development-Information/Tor-Browser/Windows-dev-builds)
- [Debugging and Triaging](Development-Information/Tor-Browser/Debugging-and-Triaging)
- [How to Contribute Patches](Development-Information/Tor-Browser/Contributing-to-Tor-Browser)  
- [Reproducible Builds](Development-Information/Tor-Browser/Building)
