# Documentation Hackweek Ideas

## Facilitator(s):

We are going to have a documentation hackweek between November 6 and 10.
Do we have any ideas to present for the applications team?

## Possible ideas

### [PV] Linearization of the patch set

We have a peculiar development model, which makes it hard to understand when a change appeared or a patch has been modified in a certain way.

My proposal is to have a second repository only with .patch files (one for every commit) that we could use to find when a certain patch has been added.

It could be automated to some extent (e.g., after applying a patch we should obtain an identical tree to the one of the original commit).

I'm not sure it fits the hackweek purpose (it's documentation, but not in the traditional meaning).

### [PV] Survey and document tor-browser-build

We have 120 projects in tor-browser-build.
It's easy to understand why we have them (e.g., Firefox), but some other are platform-dependent (e.g., the shenanigans that macOS signing needs), some other are Namecoin dependencies (I expect about 36 projects that start with go to be used only by Namecoin at this point).

I think it'd be great to produce a map of the dependencies and to explain what each project does (e.g., we understand that we need browser package to prepare the packages, but why isn't it the same as the release project, besides technical reasons such as using containers? What are all the steps we do there? E.g., it'd be nice to write somewhere that customizing Firefox prefs to include the defualt PTs is one of the things we do there).

Ultimately, this could be used to do a cleanup of the repo (e.g., are there projects we don't use?).