The [tor-browser repository](https://gitlab.torproject.org/tpo/applications/tor-browser) is based on [Mozilla Firefox](https://github.com/mozilla/gecko-dev). We apply a series of patches on top of a Mozilla Firefox branch: changing some of the Mozilla code to adjust its behaviour, and adding some of our own additional components.

The tor-browser repository only forms the "firefox" component for the [tor-browser-build repository](https://gitlab.torproject.org/tpo/applications/tor-browser-build), which is used to build the full Tor Browser application for distribution. Roughly, tor-browser contains everything needed to build the basic browser application for desktop, but does not include other needed components like the actual `tor` program. Tor Browser for Android also uses the tor-browser repository as a build dependency, but most Android-specific components for Tor Browser are found in the [firefox-android repository](https://gitlab.torproject.org/tpo/applications/firefox-android).


# Releases and Branches

Tor Browser is based on Mozilla Firefox ESR (Extended Support Release). When we want to target a new ESR minor or major release, we re-apply our Tor Browser patches on top of the corresponding Mozilla Firefox branch, which is often referred to as an *ESR rebase*. As such, we don't maintain a "main" development branch for tor-browser, and instead we have separate branches based on releases. These are named with the pattern

```
tor-browser-<firefox-base-version>-<tor-browser-series>-<rebase-number>
```

For example `tor-browser-115.4.0esr-13.5-1` is a branch based on the Mozilla Firefox 115.4.0 ESR branch, destined for a 13.5 Tor Browser release, and this is the first rebase for this combination (additional rebases for a fixed combination would be rare). If you inspect [the commits for this branch](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commits/tor-browser-115.4.0esr-13.5-1), all the top commits are used for Tor Browser, up to the `FIREFOX_115_4_0esr_BUILD1` tag, which comes from a Mozilla Firefox ESR branch.

Generally, new commits will target the branch that is marked as ["default" on gitlab](https://docs.gitlab.com/ee/user/project/repository/branches/default.html), which will sometimes change after an ESR rebase. The nightly releases typically target the tip of this default branch, whereas the alpha and stable releases will target specific commits in one of these `tor-browser-` branches, marked with a corresponding "build" tag. The latest alpha tag tends to be in the same Tor Browser series as the default branch, whereas the latest stable tag will tend to be `0.5` earlier in the Tor Browser series (so 13.5, if the alpha release is at 14.0).

Our ESR rebases for the release channel take place as soon as possible after Mozilla publishes their `_BUILD1` tag, which usually happens about a week before the corresponding Firefox ESR version is released. You can check the [Firefox ESR release schedule](https://whattrainisitnow.com/release/?version=esr) and the [Tor Browser release schedule calender](https://nc.torproject.net/apps/calendar/p/Dy5spytzmYoJPieT) to get a sense for when these will take place.
The branch for the stable release is usually rebased a little bit later.


# ESR Rebase and Commit Structure

As we develop Tor Browser, we try not to interlace our changes with the changes coming from Mozilla.
Instead, we keep our commits distinct, and apply them *after* Mozilla commits.
Whenever Mozilla releases a new version (including periodic updates) we perform an *ESR rebase*, where we take our Tor Browser commits from a previous `tor-browser-` branch and apply them on top of the new Mozilla Firefox ESR base. This can lead to conflicts, especially when changing between major ESR versions, which we resolve case-by-case by amending our Tor Browser commits.

This pattern means that we cannot practically maintain a full history of all the changes for Tor Browser alongside a changing ESR base. If we tried to apply the full history of incremental commits every time we performed a rebase, we would run into constant conflicts for each commit, many of which would be outdated or hard to resolve. Instead of using commits to represent incremental changes, we use commits to represent "features" and any changes to these features are applied as ["fixup!" commits](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---fixupamendrewordltcommitgt).

For example, a [commit for the Tor circuit panel feature](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/1b54237b803f465c3bd799c648f0758e73bc749b) will be tweaked with [a fixup commit](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/ed56d740590dd2f2596e92911c6081c70520f04c). In a later `tor-browser-` branch, the change in the fixup will be merged with the original commit to form a single commit again. This allows us to constantly clean up or adjust our patches in a way that makes the next ESR rebase feasible.

As a result, most new commits that land will be "fixup!" commits, or "squash!" commits. Exceptions to this would be for new features, specific patches we want to pull in early from Mozilla upstream, or patches that we expect to be temporary for whatever reasons.

What counts as a distinct "feature" often comes down to the question of what makes our life easier as developers, so may require some discussion. For example, [one commit is for Tor Browser strings](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/c5f069ae06c52827cccbbdb0b1c5b5fcee59cfba) even though the strings are used in different components: we made this decision because we wanted the strings to share the same file to make translation easier. Occasionally, we will change our mind and will split or merge commits to re-adjust. 

A downside to this approach is that tools like `git blame` or `git bisect` are less useful than in other projects. Generally, we compensate for this by waiting two rebases before merging the fixups into their target so that these tools still work for recent changes. We can also compensate with some clear comments describing intent or motivation with references to gitlab issues.


# Base Browser

In addition to the `tor-browser-` branches, we also have corresponding `base-browser-` branches that follow the same naming convention. The `base-browser-` branches contain the commits that are shared by both Tor Browser and [Mullvad Browser](https://gitlab.torproject.org/tpo/applications/mullvad-browser). I.e. it contains all the changes that are needed for both browsers, minus the specific changes, such as features that use the Tor network.

After we perform an ESR rebase, we create the corresponding `base-browser-` and `tor-browser-` branches. Both will share the same ESR basis, followed by an identical series of commits that form Base Browser. This makes up the entire `base-browser-` branch, whereas the `tor-browser-` branch will follow this up with its own series of commits specific to Tor Browser. After this, any patches that land in `tor-browser-` that also need to reach `base-browser-` will be landed there as well.

To sum up, we keep 4 active branches at the time:

- Tor Browser, alpha+nightly channels (the default branch)
- Tor Browser, release channel
- Base Browser, nightly channel (primarily a development tool to help during the rebases; nightly builds are built from this branch, but only to catch build-time errors, they should not be used)
- Base Browser, "release" channel (no actual release is built from this branch, it is just a development tool)


# Patchset structure

In January 2022, when the 11.5 series was in alpha, we decided to reorder the patchset in a way that was different from the previous series.

We decided a [new order](https://gitlab.torproject.org/tpo/applications/tor-browser/-/merge_requests/242#note_2769048) for the commits.
Later, during the same year, when we started working on Mullvad Browser, we had to change it a little bit.

Currently, the base browser part of the patch set looks like this:

1. All Mozilla commits up to the `FIREFOX_...` tag we're using
    * Notice that this tag exists only on the Mercurial side, so we find the equivalent commit in the official git mirror and sign it with one of our keys
    * As a result, all the Firefox ancestors in our branches have the same hashes you can find gecko-dev, which guarantees we're not messing them with hidden patches or something similar
1. Mozilla commits that we cherry-picked but did not make into the current ESR
    * backported security patches
    * commits we have uplifted
1. reverts of Mozilla commits
    * currently, we do not have any, but we had a revert commit in the 91.x and 102.x based browser because it caused a use after free, and reverting it was a much simpler option
1. uplift candidates
    * commits that we intend to uplift but have not been accepted yet or we have not even started the process
1. revert undesirable Firefox patches
1. build configuration
    * `mozconfig`s and so on
1. browser configuration
    * `001-base-profile.js`
1. tweak how the browser works
    * patches to existing Firefox code, such as security and privacy patches
1. new features
    * new identity, security level, etc... 

Then, it is followed by the Tor Browser part:

1. build script and GitLab templates
1. strings and branding
1. configurations (build and profile customization)
1. tor integration (connection, settings, etc...)
1. updater patches
1. onion services

This order applies up to `-build1` tags (the ones we create right after a rebase).
After `-build1`, there are new developments, and commits are simply in the order we merged the patches.
We try not to force push, except for rare and (extremely) exceptional circumstances.

Keeping the patchset in order is very hard.
We try to restore consistency during the rebases, but every now and then we need to stop and do additional rebases to restore it and do some cleaning.
It almost never makes sense to do more rebases in other situations, since Mozilla releases are scheduled every 4 weeks.

# GeckoView

GeckoView is a replacement for Android WebView built on top of Gecko, Mozilla's web engine.
It is used as a base for all Mozilla Android browsers.

GeckoView lives in the same codebase of Firefox.
Therefore, for Tor Browser for Android, we build a patched GeckoView with the same patches we use for desktop.

Mozilla does not offer an official ESR channel for GeckoView, so we need to backport some GeckoView security patches that are not uplifted to ESR because of this reason.

We started this "custom" ESR channel with version 12.0/Firefox ESR 102.0 because of capacity problems.
Up until the 11.5 series, we used the rapid release, therefore we had two additional branches used only for Android.
They used the `geckoview-` prefix.