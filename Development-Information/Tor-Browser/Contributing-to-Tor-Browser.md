If you wish to make a change to the [tor-browser repository](https://gitlab.torproject.org/tpo/applications/tor-browser) that you want to be accepted into the main release of Tor Browser, you can follow this guide.

NOTE: This guide will give a specific `git` work flow, which you can follow as a recipe for contributing. This is not the only flow that would work, so feel free to change any steps in a way that works best for you.

# One Time Set Up

If you do not have them already, you will need a local copy of the tor-browser repository, and your own gitlab fork.

## Fork

To create a new gitlab fork for yourself, you can visit [https://gitlab.torproject.org/tpo/applications/tor-browser](https://gitlab.torproject.org/tpo/applications/tor-browser) and follow the [gitlab guidance on forking](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html). If you are going to make regular changes to tor-browser, you may wish to give yourself SSH access to your fork using the [gitlab settings](https://docs.gitlab.com/ee/user/ssh.html). Otherwise, if you want to use git over HTTPS you may use [gitlab personal access tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html).

## Local Git Repository

To create the local git repository, you will want to clone a tor-browser repository. The repository is of order 10GB, so it may take a while to copy.

Open your terminal in the directory you wish to work in, and start with:

```sh
# Copy the main tor-browser repository. This may take a while.
git clone https://gitlab.torproject.org/tpo/applications/tor-browser.git
# Move to your new git repository.
cd tor-browser
```

This will set up a new local git repository, with the main tor-browser repository as its remote "origin". You will also want to add your own gitlab fork as another remote repository:

```sh
git remote add <remote-nick-name> <fork-URL>
```

where `<remote-nick-name>` is the name you wish to use for your gitlab fork, and `<fork-URL>` is the URL you will use for pushing and fetching from the gitlab fork. This is the URL you would use for cloning the fork in gitlab: in your fork's gitlab page, click the "Clone" button to copy the URLs for SSH or HTTPS.

### Alternative Approach

Alternatively, you way wish to instead set up your own fork as your remote "origin" (to be used by default in git push and pulls), with the main repository as an "upstream" remote:

```sh
git clone <fork-URL>
cd tor-browser
git remote add upstream https://gitlab.torproject.org/tpo/applications/tor-browser.git
```

Whilst this approach would work just as well, for the rest of this guide we will assume the other approach was used.

## Configure Git

Finally, you should configure your local repository:

```sh
# Set the name and email that should appear for your patches.
git config user.name <your-name>
git config user.email <your-email>
# In tor-browser, we use a rebase pattern.
# New branches default to rebasing during a pull.
git config branch.autoSetupRebase always
```

# tb-dev Tool

In the tor-browser repository, we have a python tool - `tools/torbrowser/tb-dev` - for some common tasks that are unique to Tor Browser development. The name of the script is short for "Tor Browser Development". The `tb-dev` scripts mostly wrap some `git` commands in a way that works conveniently for the tor-browser repository.

You can list all the commands with `tb-dev --help`, and we'll mention some specific ones below. If you use the tool often, you may wish to place `tb-dev` in your `PATH` environment somehow, such as creating a symbolic link to it in `~/.local/bin`.

# Creating a Patch

## Choosing a Gitlab Issue

All patches for tor-browser should have a corresponding gitlab issue. If you wish to make a change that isn't for an existing issue, then first open up a gitlab issue describing the problem you are trying to fix. You will likely want to wait for some input from the development team on the issue before you start doing any work.

NOTE: For tor-browser, we assign issues to developers who are planning to work on an issue, and we add the ["Doing" label](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues?label_name%5B%5D=Doing) once they are actively being worked on. You should avoid working on an issue that is already assigned to a developer, and ask what the status of the issue is before doing your own work.

## Creating a Branch

You will want a new git branch for the changes you plan to make.

You should first determine which tor-browser branch you wish to target. For more information about branches, see [the section on tor-browser branches](./Tor-Browser-Repository-Overview#releases-and-branches), but in most cases you will want to target the branch that is marked as "default" on gitlab. You can use `tb-dev show-default` which will fetch the same information from gitlab for you.

NOTE: If we are currently in the process of a new ESR rebase, there may be a small window of time where the "default" branch still points to the older branch, but you can always [rebase your patches](#rebasing-your-commits) later if this happens.

Once you have found the name of the branch you want to target, you may wish to ensure you have the latest change in your repository:

```sh
git fetch origin
```

Next, create and checkout a new branch that tracks the branch you wish to target:


```sh
# Create a new branch.
git branch <new-branch-name> --track origin/<target-branch>
# Switch to the new branch.
git checkout <new-branch-name>
```

or in one step with:

```sh
git switch --create <new-branch-name> --track origin/<target-branch>
```

where `<new-branch-name>` is the name for your new branch, and `<target-branch>` is the branch you wish to target. The convention for tor-browser is to include the gitlab issue number in the branch name. E.g. `my-first-fix-bug-54321`, or simply `bug-54321`, or whatever helps you keep track.

NOTE: The above command will set up `origin/<target-branch>` as the basis of your changes, as well as your tracking branch. This means that a `git rebase` or `git pull` will automatically rebase your changes on top of the latest changes from the main tor-browser repository. Be aware that a `git push` without arguments would attempt to push your changes to the main repository, although this should fail without extra permissions.

NOTE: You are free to create a branch without any tracking branch. Just note that you will need to provide more arguments for a `git rebase`, and many of the `tb-dev` scripts rely on a tracking branch being set.

Alternatively, you can perform the above using just:

```sh
./tools/torbrowser/tb-dev branch-from-default <new-branch-name>
```

This will perform the above steps using the "default" branch on gitlab as the tracking branch.

## Commit Your Changes

Unlike most git projects, we perform regular ESR rebases for tor-browser, which means most changes should be committed as fixups of existing "feature" commits from the "origin" branch. For more information, see [the section on our commit structure](./Tor-Browser-Repository-Overview#esr-rebase-and-commit-structure).

If you are contributing an entirely new feature to Tor Browser, you can create a new commit. But in most cases, you should determine which existing feature commits your changes should target with a fixup.

For example, suppose your changes involve modifying both `browser/components/newidentity/content/newidentity.js` and `browser/components/BrowserGlue.sys.mjs`. If we look at the [history of changes for `newidentity.js`](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commits/tor-browser-115.4.0esr-13.5-1/browser/components/newidentity/content/newidentity.js), this is an entirely separate feature added for Tor Browser. The only normal commit that changes this file has the message ["Bug 40926: Implemented the New Identity feature"](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/bd30dffe843051a098ddf0107edbd4d9eca5b08c), with a commit hash of `bd30dffe843051a098ddf0107edbd4d9eca5b08c`. For a situation like this, we most likely want to fix up this commit:

```sh
# Stage only the changes to newidentity.js
git add browser/components/newidentity/content/newidentity.js
# Verify that only newidentity.js is staged.
git status
# Create a new fixup for "Bug 40926: Implemented the New Identity feature".
git commit --fixup=bd30dffe843051a098ddf0107edbd4d9eca5b08c
```

Next, you will often want to add your own message for what changes you made. To add your message, use:

```sh
git commit --amend
```

and append lines for your message, so that the overall message would look something like:

```gitcommit
fixup! Bug 40926: Implemented the New Identity feature

Bug <issue-number>: Description of my changes.
```

where `<issue-number>` is the gitlab issue number for the issue you are trying to fix.

NOTE: A "fixup!" message will be lost once the changes eventually merge into the main commit at a later ESR rebase. This is desirable in most cases since we want the commit message to remain descriptive of the overall intent of the commit. If you wish to append your message permanently to the main commit's message, use `--squash` instead of `--fixup` above to create a "squash!" message.

In contrast, if we look at the [history of changes for `BrowserGlue.sys.mjs`](https://gitlab.torproject.org/tpo/applications/tor-browser/-/commits/tor-browser-115.4.0esr-13.5-1/browser/components/BrowserGlue.sys.mjs), it contains changes from several Tor Browser commits, as well as from Mozilla. In a situation like this, you should review each existing commit and determine whether your changes relate to any specific one. For example, if you change a line that was added in one commit, then that would be an obvious target. If none of the commits relate to your change, but they are relevant for some other "feature" commit, you could target that instead. In general, use your best judgement and discuss with other developers if you are not sure.

We have an automated tool to help you make these decisions:

```sh
./tools/torbrowser/tb-dev auto-fixup
```

This will take each changed file in the current working directory and make suggestions about which commit to target. It will then create the fixup commits for you, and allow you to add any messages.

If you are creating, deleting or moving entire files, you may need to take the manual approach above. Similarly, if you are making changes to a single file that may need to fixup several "feature" commits, you may want to use `git add --interactive` to have more fine-grained control.

In these situations, you may find it useful to use `tb-dev log` to run `git log`, but limit the commits to only changes made for Tor Browser. I.e. excluding the long history of changes that come from Mozilla, which should never be fixed up. For example:

```sh
# View the history of changes to BrowserGlue.sys.mjs
./tools/torbrowser/tb-dev log -- --patch -- browser/components/BrowserGlue.sys.mjs
# View what files each commit has targeted.
./tools/torbrowser/tb-dev log -- --stat
```

## Code Comments

Where relevant, please comment your code with motivations. This is advised for any coding project, but for tor-browser it can be especially useful because our fixup structure can make diagnosing *where* a change came from difficult.

Often, you will want to include a gitlab issue number in a comment. For tor-browser issues, please use the structure "tor-browser#54321", replacing "54321" with the gitlab issue number. If you want to reference a bug from Mozilla's bugzilla, please use "bugzilla bug 54321" to distinguish it. Note that Mozilla uses simply "bug 54321" to reference their own bugs.

## Linting

We borrow the linting from Mozilla for tor-browser as well. However, as an exception, we do not apply the linting rules from Mozilla for adding the Mozilla Public License to the top of files. You can lint all of your changes using:

```sh
./tools/torbrowser/tb-dev lint-changed-files
```

to verify whether your changes need linting. You automatically resolve some issue by running:

```sh
./tools/torbrowser/tb-dev lint-changed-files --fix
```

Once the linting is complete, you may need to re-commit any changes.

If you wish to run a specific linting tool, you should use the `./mach lint` tool directly. For example, to just run the `eslint` linter:

```sh
./mach lint -l eslint <path-to-file>
```

## Testing your Changes

You should always try and test your changes before opening a merge request and asking for review. Most of the time, you can use a ["dev" build](./dev-Build) to test your changes manually in Tor Browser. Also, check the [debugging section](./Debugging-and-Triaging), which can also be used the verify your changes, and to diagnose any problems.

# Submitting your Changes

Once you are happy with your changes, you can create a merge request on gitlab targeting the main tor-browser repository.

First, ensure you have run the linting tool and that all of your changes are committed to your new branch locally. Next, with your new branch checked out, push it to your gitlab fork:

```sh
git push <remote-nick-name>
```

where `<remote-nick-name>` is the git remote name you gave your gitlab fork. When you push your change, gitlab should respond in the terminal with a link to open a new merge request. Otherwise, you can [open one manually](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

From here, you can fill out the merge request form and create it. Make sure that the target of your merge request matches the tracking branch you used above when creating your new branch. Gitlab will automatically choose the "default" branch, but if you are targeting a different branch then select it instead.

# Making Adjustments

If you need to make adjustments to some of your existing commits, then you will likely want to fixup your current patches.

Usually, you can make your further changes, and then run:

```sh
./tools/torbrowser/tb-dev auto-fixup
```

In this case, you will likely want to target your previous fixup commits with a second fixup instead. I.e. you will end up with commits like "fixup! fixup!".

You should then use an interactive rebase to merge these changes into one. Since git's `autosquash` feature does not handle "double" fixups, you can instead use:

```sh
./tools/torbrowser/tb-dev clean-fixups
```

to perform an interactive fixup where the "double" fixups are automatically converted into fixups of the "single" fixups. You should review the result before confirming.

For example, consider a local branch where we have already made a change to `newidentity.js` in a fixup commit with the message:

```gitcommit
fixup! Bug 40926: Implemented the New Identity feature

Bug 54321: Description of my changes.
```

Next, we lint the file or make a change in response to a gitlab review. Since we have made some further changes to `newidentity.js`, we create a new commit with the message:

```gitcommit
fixup! fixup! Bug 40926: Implemented the New Identity feature
```

Either with an interactive rebase, or using `tb-dev clean-fixups`, we perform a rebase with the following content:

```gitrebase
pick eafe5220402b1 fixup! Bug 40926: Implemented the New Identity feature
fixup 9258ed2aa7d00 fixup! fixup! Bug 40926: Implemented the New Identity feature
```

As a result, we again have a single commit, with the changes of both, and the message:

```gitcommit
fixup! Bug 40926: Implemented the New Identity feature

Bug 54321: Description of my changes.
```

Once your rebase is complete, and you are happy with the changes, you can push the changes to gitlab with

```sh
git push -f <remote-nick-name>
```

The `-f` argument will force the push to go through after a local rebase, and overwrite the remote branch. Any existing merge request on gitlab that sources this branch will be automatically updated.

Gitlab will automatically mark merge requests with new fixup commits as a "Draft". Currently there is no way to switch this behaviour off, but there is an [open issue to request it](https://gitlab.com/gitlab-org/gitlab/-/issues/337106). For the time being, you will need to manually remove the "Draft: " prefix from the merge request title after your push.


# Rebasing Your Commits

At times, you may wish to apply your changes to a new basis.

If your origin tracking branch has since had commits added, then to re-apply your changes on top run:

```sh
# Fetch the changes from origin.
git fetch origin
# Re-apply your local branch on top of its tracking branch.
git rebase
```

or simply in one step as:

```sh
git pull
```

You may run into merge conflicts, which you should resolve appropriately.

If instead you wish to target the newest "default" branch after an ESR rebase, you can use the following tool:

```sh
./tools/torbrowser/tb-dev move-to-default
```

If there is a new "default" branch on gitlab, this will move your current local branch on top of it, and set the new default as its tracking branch instead. The old local branch tracking the previous default will be saved under an altered name, which you can delete if you no longer require it.

In other situations, or if you do not have a tracking branch set up, you may need to use a more complex set of `git` commands.

NOTE: If you have an open merge request, and it requires both a rebase on the tracking branch, as well as other adjustments to the patches, first perform only the tracking branch rebase and push the local branch, then apply your adjustments and push again. Keeping the rebase and the adjustments as separate pushes can help the reviewer distinguish between changes due to one push or the other.