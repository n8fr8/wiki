Here are a list of links that you can try out for triggering security warnings on the Tor Browser. For clearnet warnings, visit [badssl.com](https://badssl.com/):

- Expired certificate: [expirezf7z2sqlnqtlnmxhy5dhiykby46v5vpp6tkm3odix3ojikpxid.onion](http://expirezf7z2sqlnqtlnmxhy5dhiykby46v5vpp6tkm3odix3ojikpxid.onion)
- Time Issues: [future2eb4tqrw6t6ka5ekdrcw5mjaxqu4rayndk775cdi6aihod7yyd.onion](http://future2eb4tqrw6t6ka5ekdrcw5mjaxqu4rayndk775cdi6aihod7yyd.onion)
- Security Risk: [domain736gp5nirxh4iypjf2m42cy5sipj5vnecvexag6svvsi52ovyd.onion](http://domain736gp5nirxh4iypjf2m42cy5sipj5vnecvexag6svvsi52ovyd.onion)
- Self-signed Cert: [pierovlcy7baatz7xcaccbf2kanct3hvkhcgedz4cbrmcg2ybmjalxad.onion](http://pierovlcy7baatz7xcaccbf2kanct3hvkhcgedz4cbrmcg2ybmjalxad.onion)
  - Private key: `VQCRB3BB56UZV7ZOUAHVAIV3UUPWHII3WM5RCZOSMDJKDREPDQGQ`