

# Tasks and Timeline
## Milestone 1: Dec 23rd
### Completion of the Alpha release of Security Slider configuration component.
#### sept - oct 2016
 * Review Tor Browser "Tor Button" code including Security Slider (More info on TB Security Slider at #9387)
   * !https://trac.torproject.org/projects/tor/wiki/doc/UX/OrfoxSecuritySlider
 * Begin to port Security Slider code to Android software platform. (Ticket for FE dev #20815)
   * create ux for security slider on mobile (wireframes)

#### november 2016
 * Continue porting Security Slider code for Android platform.
   * created FE code based on wireframes
 * Identify tasks for future porting of other desktop Tor Browser "Tor Button" functionalities to mobile Tor Browser.
 * Identify candidate UX workshop partner organizations in target countries - **DONE - India - Brasil  (maybe Colombia)**

#### december 2016
 * Contact UX workshop partner candidates and confirm availability (Brazil, India).
 * Continue developing Security Slider functionality and user interface components on Android.
   * Finish FE
   * Work on BE

## Milestone 2: 27 Jan 2017
### Completion of stable release of Security Slider configuration component.
#### Jan 2017
 * Complete porting Security Slider code to Android.
 * Perform software QA (functional and integration tests) on Security Slider functionality.
 * Finalize logistics of UX workshop meetings.

## 
## Milestone 3: 28 Feb 2017
### Completion of Usability Workshops in target countries.
#### february 2017
 * Provide materials, guidance, and administrative and logistical support for UX testing partners.

## 
## Milestone 4: 31 March 2017
=== a) Completion of usability improvements from workshop feedback.
b) Submission of the Final Report of all activities and results. ===
#### march 2017
 * Review, triage, and functionally translate feedback from UX workshops.
 * Fix "Level 1" UX findings (visible errors and significant impediments to usability) in the Security Slider component of the mobile browser.

== ==
## Rome Roadmap - 2018
|= month =|= Type =|= Tickets/proposals =|= Description =|= Who? =|= Depends on =|= is a dependency of =|
|---------|--------|---------------------|---------------|--------|--------------|----------------------|
| **March** | | | | | | |
| | Release/Sponsor8 | | Get a new Orfox release out | igt0/sysrqb | | |
| | Sponsor8 - alpha blocker | #21863 and children | Investigate possible proxy bypasses | igt0/sysrqb | | |
| | Sponsor8 | | Review of play store stats (install/user stats/usability feedback) (O1.1) | isabela | | |
| | Sponsor8 - alpha blocker | #25603 | Update Orfox patches and extensions | sysrqb | | |
| | Sponsor8 - alpha blocker | | Test fingerprinting/linkability defenses (O1.2) | igt0 | | |
| **April** | | | | | | |
| continue from March | Sponsor8 - alpha blocker | #21863 and children | Investigate possible proxy bypasses | igt0/sysrqb | | |
| continue from March | Sponsor8 - alpha blocker | | Test fingerprinting/linkability defenses (O1.2) | igt0 | | |
| | Sponsor8 - alpha blocker | | Create Tor Browser for Android branch based on mozilla-central | igt0/sysrqb | | |
| | Sponsor8/tbb-desktop | #24856 | Integrate Tor Launcher as a systems add-on | sysrqb | ux team needs to do all design work for this? or this can start without design work | |
| | Sponsor8/tbb-desktop | #24855 | Integrate Torbutton as a systems add-on | igt0 | ux team needs to do all design work for this? or this can start without design work | |
| **May** | | | | | | |
| | Sponsor8 - alpha blocker | | Branding for the new Tor Browser for Android alpha | igt0/sysrqb | ux-team and communications | |
| | Sponsor8 - alpha blocker | #25696 | Design of alpha onboarding for Tor Browser for Android | igt0/sysrqb | ux-team | |
| Continue from April | Sponsor8 - alpha blocker | #21863 and children | Investigate possible proxy bypasses | igt0/sysrqb | |
| | Sponsor8 - alpha blocker | | Test fingerprinting/linkability defenses (O1.2) | igt0 | |
| | Sponsor8 | | Refactor Torbutton to allow mobile UI | igt0 | depends on ux team | |
| | Sponsor8 | | Refactor Tor Launcher to allow mobile UI | sysrqb | depends on ux team | |