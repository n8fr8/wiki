# AArch64 support for Linux

## Facilitators: PieroV?

## Rationale

- It seems to me that requests for aarch64 are getting slightly more frequent, and maybe cross compiling isn't that hard after all (are multiarch + Clang enough?), but it surely would require us a certain amout of time to set it up (go through each project to add the cross-compile flags).
- Also, Tor Browser builds don't take that long now (multilocales was a huge help, and recent hardware is very fast). Plus, signing for Linux is just pgp-signing, it doesn't involve complex procedures.
- Firefox lists Linux/AArch64 as a tier 2 platform, like Windows Mingw x86_64.
  - https://firefox-source-docs.mozilla.org/build/buildsystem/supported-configurations.html#tier-2-targets
- Tor and PTs already work on AArch64 on Android and macOS... so no big deal also for Linux, right? For little-t-tor people wrote it works without any issue, for PTs I'd guess Go is taking care of all the low level real problems.
- Hardware for us to test/debug is the biggest concern for me.
- This gave me the idea of the tor-browser-build docs+survey for the hackweek :)
- If Mullvad is interested (they already supports aarch64 for the VPN) we could start with Mullvad Browser, which uses less tor-browser-build projects.

## Non-goals

- AArch64 Windows
  - Less users than Linux AArch64? I've seen only one request recently, because 13.0 broke on ARM.
  - Tier 1 for Firefox but with clang-cl. Unknown state with mingw.
  - Even more difficult hardware choice? Full laptops, whereas for Linux we might be lucky and use sbc
- ARM 32 bit (e.g., ARMv7 and other ARM variants) (officially)
  - I'd expect them to be low-end hardware by now (a bootstrapped Tor Browser showing only about:tor takes about 450MB on my Linux x86-64 computer)
  - Jeremy worked on this and found some pain points