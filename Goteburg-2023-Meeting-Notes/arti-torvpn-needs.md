# Tor Browser Need from Arti and TorVPN

## Facilitator(s): pierov
## Who attended this session: 
- geko
- pierov
- ahf
- eta
- ma1
- clairehurst
- dan_b
- micah

## Topics

- TBA + TorVPN planning
- Review of the TorProvider in context for Arti (and maybe also for the TorVPN)
  - E.g., we could give the net team the list of the functionalities we need - we're slightly late but now we have an official one :)
  - Biggest mystery of the refactor: do we still need newnym? What does it do exactly?
    - We got some answers already! https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41708#note_2953080

## Goals
- Plan for implementation of connect-assist frontend
  - native-Android OR about:connection
- Short-term plan for implementation of TorProvider
  - use existing legacy tor daemon and share control port info w/ GeckoView backend OR Arti
- Long term plan for implementation of Arti-based TorProvider

## Notes

Don't want to do custom network code, would like something that is reliable.

Arti the binary does SOCKS, but arti the library does not provide SOCKS tools at the moment. That could be done by moving some code around. The only way to use arti right now is to use the rust API... you will need to write a SOCKS server and run that. There is a tor-socks-protoserver crate that does a lot of that for you, but you want to do it on your own. 

Would like to keep the user/password SOCKS isolation.

Depends on how you structure things:
- configure firefox to use SOCKS
- when new SOCKS connection came in, use existing api to figure out which tab its related to:
  - you would want first-party isolation, not tabs instead of using username/password (becaause you will control both sides of the socks client and server) you wouldn't need to marshall the authentication piece. 
  - generate for each connection a new socks username (don't care about the password, as this can be done with a call into the rust side of things, just ask arti to make a new circuit), and use the username to correlate the connections. Store that in the table for looking up, do some FFI things and multiplex things on the rust side. 

**alex**: next thing for arti work is attaining feature compatibility for what the browser needs. when we talk about the end goal being where we want to work with the VPN... at that point we should have RPC where you can have a proxy object that you can control and receive messages. Maybe you want to have call backs when there are events, so that needs to be tied to an event loop, so either you have to have things running into a thread, or its tied into the FF event loop. 

**pierov**: we try to do as little native code as possible

**ahf**: are the event loops the same in desktop and android?

**dan**: no, its not the same... we inherited the tor android services from GP and a tor controller.

**ahf**: we need to tie in file descriptors at an entry point somewhere

**dan**: tor android service emits events
should be able to wrap a few calls if we need to.

net team needs to take from here what arti needs to provide

## Further references

- Tor Provider and all the Tor stuff on Tor Browser now: toolkit/components/tor-launcher

    - Especially interesting: TorProvider.sys.mjs

- Passing messages on Android: mobile/android/modules/geckoview/Messaging.sys.mjs