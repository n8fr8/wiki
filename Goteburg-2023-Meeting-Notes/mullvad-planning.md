# Mullvad Planning

## Facilitator(s): rui 

## Issues
- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42227
- https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42224


## Topics

### TZP refactor

https://arkenfox.github.io/TZP/tzp.html

Things haven't continued after the first attempt.
Rui would like it to continue to test all the new releases.
Two problems:

1. everything is only in Thorin's head [clone head in jar aka futurama]
2. at the moment we don't run the tests (the current version of marionette has some difficulties, but there's torbrowser-selenium that we could try to use)


If we wanted to start with the non-refactored version already, we could do it probably soon. TZP already has a global object that contains the recap of the test.
We could run TZP in torbrowser-selenium or marionette, and get that object already. We just don't know how long it could take (PieroV expects not that much).

The long term goal is to have a library both for this and for the in-browser page that tells the user that something is wrong.

Currently used for:
- make sure we don't have regression/check ESR updates
- collect unique fingerprints to check how many buckets a certain vector might end up creating


We could find a way to collect entropy with an invite system (you visit the site only once, so that you don't mess up with our stats - no PII data collected, of course).

Currently TZP is only at the document level, but we should add iframes (PieroV: both on FP and TP), workers, etc... There is a lot that could happen.
When things are modular, we can add these tests.

The refactor isn't only about the code, it's also about adding documentation.

Potential source for devs: https://jshelter.org/
Could potentially look through the old browser resumes

### Longer term stuff

- Browser sanity page
- Documentation on the various browser settings
  - Does it affect fingerprint? What unexpected consequences could it have?
  - Enable/disable some settings depending on the dynamic threat model
- Traffic analysis stuff? Not sure what we are expected to do on that
- Web authentication (not so long, 13.5 pretty please)
- Launching third-party apps (not so long, also 13.5 maybe)
  - Thorin says to tie this to the dynamic threat model
- Some UX work on how to add all these settings? Not planned at the moment, but we might need one eventually
- New packages (.deb, .rpm, also not so long probably)
- Service workers (depends on upstream to partition them correctly and prevent disk leaks for PBM)
- Login persistance/local storage for some sites
  (lots of stuff said here, sorry, I have no notes)
  - disable always-on PBM but keep the first window PBM (suggested by ma1, implementation detail for Rui)
  - use containers in "persistent" windows
  - possible UX problems to figure out first
  - user complaints:
    - users want some exceptions for login persistance in Mullvad Browser
  - Mozilla is trying to do stuff on PBM (e.g., on Windows there's the privatebrowsing.exe proxy to the launcher process, then they're trying to make the PBM windows in separate groups, so they're indeed doing something to make PBM mode different, so it'd be great to contact Mozilla and understand their plans here)
- Sync alternative (a peer to peer one, Rui is already having Ike creating a web extension for that)
  - Quality of life improvement, rather than a real priority
- What about Mullvad Browser for Android?
  - ASAP from a certain point of view, many users are on mobile and would like to have it
  - Depends on patch cleanup work planned for end of 13.5
  - But RR first?