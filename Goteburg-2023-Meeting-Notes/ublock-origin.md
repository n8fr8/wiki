# uBlock-origin or uBlock-origin-lite in Tor Browser

## Facilitator(s): ma1

## Topics

- ublock-origin lite mode or w/e it's called
  - some discussion here: https://gitlab.torproject.org/tpo/applications/mullvad-browser/-/issues/164#note_2947346

- old issue for Tor Browser: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17569

## Todo:

- sync w/ Tails devs re what block lists they are using so we can converge/have the same thing
- we would want to hide the ublock UI
  - sync w/ gorhill (ublock maintainer) re exposing apis for us to use in the browser
- sync with the blocklist maintainers over differences between lists/etc

# Benefits
- improved network perf
- removed malware vector
- no ads!
- convergence w/ Tails

# Drawbacks
- filter-lists become censorship vector, we don't control the lists
  - more 3rd parties
- hostility to websites means they have no incentive to be tor-browser compatible
- ethical thing about advertising revenue
- filtering is not compatible by design (enumerating badness is bad, define a proper solution)
- fingerprinting (if block-lists aren't in sync, custom user filters, etc)


## Goals
- Determine if we will integrate ublock-origin into Tor Browser (hopefully informed by the threat-modeling discussions)
  - definitely maybe

- Determine *which* flavor of ublock-origin to integrate
  - classic (disable CNAME+cloaking)
  - not lite (lite not as effective w/ google's v3 thing, so not a practical solution)