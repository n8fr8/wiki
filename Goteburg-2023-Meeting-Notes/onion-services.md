# Onion Services and HTTPS - Tor Applications Meetup 2023 - Goteburg, Sweden - 2023-10-26

Notes moved to https://nc.torproject.net/f/514897 - please edit there.

## Participants

* richard
* pierov
* ma1
* micah
* boklm
* ...?
* donuts (remote)
* henry (remote)
* gaba (remote)
* rhatto (remote)

## Facilitators

* rhatto

## Topics

Topics we discussed:

- Onion Services chat
- Onion Services and certificates

Topics we did not discuss due to time constraints:

- Confidential status for the issues
- Alt-Svc + onion services usage is hidden from users
- Faulty Onions project

## Certificates

### Slides

* Presentation slides: https://gitlab.torproject.org/tpo/onion-services/onionplan/uploads/8d8a922a004e454c2404f02c863e5f84/tbb-qa-gothenburg-20231025.pdf

### Discussion

Some incomplete notes from the discussion:

* What HTTPS adds for .onions:
    * Not much in terms of encryption, but people still need and use HTTPS.
    * But has it's benefits: https://tpo.pages.torproject.net/onion-services/onionplan/proposals/usability/certificates/#benefits

* Current behavior:
  * We're currently check that the certificate has the .onion address; sounds like we're already implement SOOC: https://github.com/alecmuffett/onion-dv-certificate-proposal/blob/master/text/draft-muffett-same-origin-onion-certificates.txt
  * Current icon specs: https://support.torproject.org/onionservices/onionservices-5/ 
  * Seems like the current behavior was implemented at https://gitlab.torproject.org/tpo/applications/tor-browser/-/commit/06fd55e2e061c7348f86d7f0ced3d64ffac9baad
  * Tor's onion website begins with http://2gzyxa -- one could generate a vanity
address with the same starting chars, so it would be hard to tell the difference, and  the attack could be even worse. This also happens with regular sites, but takes
longer and requires more resources (buying a domain etc).

* Remarks:
  * Authencity is important for HTTPS, as well as other security properties.
  * Warnings are interesting, may inform users, but also may lead users to think that Tor Browser is not safe.
  * But we need to be transparent somehow about the risks.
  * Regular sites can be taken down, usually not with .onions.
  * A/I is making impersonating even easier: reports of fake Firefox sites popping up on Google searches and offering adds etc. Things are getting worse in this regard.
  * Another point in the "I don't think you're getting informed consent from SSL errors" rant is that if you think back to the past 100 SSL errors you've personally seen, how many of those were actually malicious versus a technical issue on the website/cert side?
* I.e. the uncomfortable truth is that users have been trained to ignore these warnings anyway.

* Concerns:
  * Concerns about how many onion services a hard neterror would impact (which is what FF and Chrome do for self-signed clearnet sites), that aren't problematic.
  * Concerns about about centralization, maybe there's room for the middle ground.

* Misc:
  * There's the [urlbar ... lost this part mentioned by duncan]
  * Discussion on the Secure Cookies thing.
  * Custom CA tutorials: https://gitlab.torproject.org/tpo/onion-services/onionplan/-/issues/11#note_2944844
  * Current .onions for testing: https://gitlab.torproject.org/tpo/applications/team/-/wikis/Development-Information/BadSSL-But-Onion
  * And to compare clearnet sites: https://self-signed.badssl.com/

### Problem statement

* Discussion about whether we agree this is a problem.
* We need to understand the problem better before discussing the icons.
* "We agree that we have a problem, but we don't know exactly what the problem is, and we have many suggestions to mitigate this problem we don't know yeat what it is...".
* Maybe we could reframe the problem: its not exactly with the certificate, but the impersonating vector: it's easier to impersonate onionsites than regular sites; can be even easier when free-of-charge DV certs arrive.

### Suggestions

* Solving is hard, maybe mitigating is a better target, by making impersonating onionsites hard to convince users.

* Research:
  * We could include this UX research on onion services impersonation in the Onion Discovery funding proposal? Maybe, as onion discovery could try to address this issue as a byproduct. We could list as a benefit in the proposal, and try to create criteria to evaluate proposals based on how they reduce the threat/protects against impersonation.

* Development:
  * The options from the slide deck.
  * ma1's suggestion:
    * UX: very clear the difference, keep the capability for onionsites to have the Secure Context.
    * But the UX should be clear, state that cannot guarantee that the user is connecting to the site they're intended to connect.
    * Showing Common Name in the address bar.
    * Showing "Unknown" ... [in some condition] (suggestion by ma1).
    * Maybe warnings could be skipped if the site is in the user bookmarks (that would be some kind of TOFU) (some users may not be willing to use bookmarks, to reduce stored data on their devices)
  * pierov's popup idea:
    * First time popup when the user is visiting the first .onion.
    * Maybe also a "guided tour" in the popup.
  * gaba's suggestions:
    * Something similar like what ooni does in their apps to understand risk.
    * Some way of telling the users about this risk?
    * To not create a false sense of security... that's why they need to know.

* Outreach:
  * Make a survey (for onion service operators) to understand usage patterns. We've been thinking about doing a new tor browser user survey, we could have a question to fork out onion service operators and ask them specific questions. Or maybe a separate survey for operators could also be useful to understand certificate usage?
  * Try to get numbers on DV and EV certs issued, through CT Logs.

* Support:
  * Improve the docs/support portal.

### Examples

- Some examples of UIs: https://gitlab.torproject.org/tpo/applications/team/-/wikis/Development-Information/BadSSL-But-Onion
- Bentham's proposal for pet names: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40845

## Alt-Svc

### Intro

* Alt-Svc in the Onion Plan: https://tpo.pages.torproject.net/onion-services/onionplan/proposals/usability/discovery/translation/#alt-svc

### Existing issues

* Indicate alt-svc onion explicitly in the urlbar - https://bugs.torproject.org/tpo/applications/tor-browser/40587
* Display .onion alt-svc route in the circuit display - https://bugs.torproject.org/tpo/applications/tor-browser/27590
* Show onion redirects in the tor circuit panel - https://bugs.torproject.org/tpo/applications/tor-browser/41703
* non-onionsite gives interstitial "Onionsite Has Disconnected" page - https://bugs.torproject.org/tpo/applications/tor-browser/40434

### Discussion

We did not discuss Alt-Svc in this session (the certificates discussion took all the available time).