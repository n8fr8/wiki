# Password Managers in Mullvad Browser

## Facilitator(s): rui
## Who attended this session: 

- boklm
- thorin
- richard
- rui
- gaba
- henry-x

## Topics

- Password managers, what would be good solutions
- Fingerprinting >:[
    
# Notes

- ma1: so long as any PW manager feaure requires user interaction :thumbs-up: /fin
- firefox
  - builtin pasword manager
  - if you enable, creates the db etc and you can use it
  - disabling it does *not* clear old passwords
  - does not force use of a master password
  - thorin's thoughts are: 
    - builtin firefox pw manager
    - ?prompt/recommend master password 
    - new ux to enable deleting the password file
    - disable auto-fill
       - Form data can easily be stolen by third parties
         - see this 2017 [article](https://freedom-to-tinker.com/2017/12/27/no-boundaries-for-user-identities-web-trackers-exploit-browser-login-managers/) and [bugzillas 1443083 + 1427543](https://bugzilla.mozilla.org/buglist.cgi?bug_id=1443083,1427543)
         - see this 2011 [article](https://blog.mindedsecurity.com/2011/10/autocompleteagain.html) and [bugzilla 381681](https://bugzilla.mozilla.org/381681)
       - also see https://portswigger.net/research/using-form-hijacking-to-bypass-csp
          - depending on the password mgr, users starting to type in the form field may or may not trigger the mgr (I would hope that built-in browser solutions would be more robust) - not to be confused with a user typing details in anyway

- confirm w/ pierov or ma1:
  - we need certdb for the passwords
    - pref requires restart, is it ok to have is on disk always (e.g. empty)

- fingerprinting:
  - extension modify the dom, built-in does not its in the browser chrome
    - *having* a password in pw manager isn't a useful bit since it isn't stable across all websites for fingerprinting scripts
    
# TODO:
- figure out whether the firefox password manager feature modifies the DOM or not
- figure out if other extensions are fingerprintable
   - update 11-nov-2023: yes they can
     - see https://github.com/mullvad/mullvad-browser/issues/74#issuecomment-1806442479 Bitwarden modifies navigator properties (at a minimum)
     - ^ not tested, but 3rd party extensions can modify prototype/proxy and is detectable

# Curent FF UX:
- Logins and Passwords
  - Ask to save logins and passwords for websites  [Exceptions]
    - Autofill logins and passwords  [Saved logins]
    - Suggest and generate strong passwords
    - Suggest Firefox Relay email masks to protect your email address
    - Show alerts about passwords for breached websites
  - Use a Primary Password  [Change Primary Password]
  - Allow Windows single sign-on for Microsoft, work, and school accounts

