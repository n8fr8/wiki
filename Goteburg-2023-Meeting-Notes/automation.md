# Release and Rebase Automation

## Issues:
- https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41000

## Facilitator(s): richard

## Topics

- Some steps of the release preparation (e.g., check for extensions updates, write stuff in the YAML files such as the translation commit hashes, etc)
- some kind of bot to keep track of some dependencies (OpenSSL, Zlib, and all the ones we don't need to be at the same version of Moz)
  - Of course Renovate doesn't know much of our build system.
  - We could find a way to use its upstrem version source.
  - We could use debian/watch files (https://wiki.debian.org/debian/watch) from debian packages.

- Rebases
  - this could happen "easily", monitor the mozilla repository, run a rebase, do tests, start with ESR with these steps. If rebase fails, notify, if build fails, notify, then have tests that would notify. The idea would be to keep the rebases continous. When a new tag comes out, then the bot would notify us to fork our side.

- Tests:
  - Mozilla's tests are mostly failing, because of our patches. 
  - We could iterate on patches/rebases to see what fails. If preferences cause test failures, maybe that could be upstreamed?
  - if our patches are doing things that are incompatible with prefs, maybe we have to adjust our patches to take those prefs into account
  - tests maybe could be slowly fixed, we would need to uplift them
  - tests would help a lot with our ESR releases
  - we have various tests that we need to reconcile:
    - upstream tests: it seems some of these fail intermittantly
    - tests that fail because we broke functionality (failed test)
    - tests that fail because we changed something deliberately: fix or disable
  - our tests of our changes
  - Can we run mochitests outside the build environment? That would be a requirement to run tests on Windows and macOS.
  - Marionette to test: https://github.com/mozilla/gecko-dev/tree/master/testing/marionette/client
- tag notifications, etc...
- there is no automation equivalent for android
- linting on merging in gitlab
  - Can we check Mozilla instrumentations? They're integrated in phabricator.
- command/script (in the CI) for detecting which commit to fixup! when writing a later patch set. Show when a fixup is likely wrong: e.g. fixup that touches a patch that never touched that file in the past.

## Priorities

- Enable geckodriver for all platforms
- CI linting
- Tests
  - tor-browser/mullvad-browser specific tests
  - Run TZP and version the various fingerprints
  - fix+run mozilla tests (low priority) (is it even possible?)
- CI builds
- Continuous auto rebasing
  - Monitor release tags
  - Rebase the patch set
    - Tooling to automatically run diff-of-diffs and maybe range-diff
  - Shuffle the commits to the right place and force push them to a separate repo
- release prep automation
  - translation hash updates
  - dependency checking (renovate)
    - noscript
    - openssl
    - zlib
    - tor
    - go
    - manual
  - buildID updates
  - *improved* changelog generation
  - communications (qa+release emails)
  - website MR
  - blog MR
- build signing improvements: (low priority but easy)
  - per-build signing (eg Android-only) tor-browser-build#40994
- Merge Requests CI
  - Linting
  - Run a build after a MR and run tests

