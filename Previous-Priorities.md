## Priorities for 2022
- Improve Browser from feedback from users
- "Tor VPN" implementation
- Improve QA process for Tor Browser
- Sponsor work related to circumvent censorship
- Tor Browser refactoring
- More people in the team! 

## The following are our "reach" priorities for 2021

- ~~Multi-locale packages~~
- Go/Rust/Java dependency resolution
- ~~Convert torbutton into tor-browser patches~~
- ~~F-Droid presence for Tor Browser for Android~~
- ~~General webcompat improvements~~
- ~~Improve fingerprinting defenses~~
- ~~Improve sandboxing~~
- V2 Deprecation UI/UX
- About:torconnect
- Desktop ESR Transition - starts in July
- Enable HTTPS-Only Mode
- Integrate libhttps-everywhere-core (replacing https-everywhere webextension) - tor-browser#40458
- Go/Rust/Java dependency resolution: how to resolve dependencies ahead of time. 
- Improve bridge selection in tor browser

The following were our priorities for 2020

* Migrate Tor Browser for Android onto [Fenix](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/applications/fenix/-/wikis/home) and away from Fennec.
* Improve Tor Browser experience for human rights defenders under censorship.
