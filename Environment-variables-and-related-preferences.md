Tor Browser looks for certain environment variables, mainly to customize the integration with the tor daemon.

Over the years, variables and preferences have been added to the now legacy Torbutton and tor-launcher.
This page includes the ones that are still valid as of Tor Browser 12.5.

Most of the environment variables are boolean. They are parsed as numbers: `0` means false and any other number means true (including negative and decimal numbers).
Any string that cannot be parsed as a number (including the empty string and values such as `true`) **means false**.
A notable exception is `TOR_TRANSPROXY`, which is active whenever defined.

Some of these variables can be made permanent by defining equivalent preferences in `about:config`.
Both can be defined, but **environment variables will take precedence** when both are set.

# Control port settings

These values are used both to an external tor daemon and when instancing `tor` within the browser.

## Control port authentication

By default, Tor Browser spawns the tor process and it sets a random password for control port authentication.

However, the following environment variables are useful with an external tor process:

- `TOR_CONTROL_PASSWD`: the password to use for the authentication in clear text.
- `TOR_CONTROL_COOKIE_AUTH_FILE`: the path of the file used for the cookie authentication (tor writes some random content on a file, and the browser sends the content back when connecting; check tor's documentation for more information).

Both these settings work also when the tor process is started by the browser.
However, the cookie file is read before starting the process, so letting tor create it **will not work**.

## `TOR_CONTROL_IPC_PATH`

Path to a Unix socket to communicate to the control interface of a tor daemon.
It can be an existing instance, or Tor Browser will pass this path as an argument when spawning `tor`.

Equivalent preference: `extensions.torlauncher.control_ipc_path`.

IPC can be enabled without explicitly setting a path when `extensions.torlauncher.control_port_use_ipc` is  set to `true`.

IPC environment variables are checked first, and preferred if `TOR_CONTROL_HOST` and `TOR_CONTROL_PORT` are defined, even though the default behavior is to use TCP.
However, if IPC is configured only through preferences but `TOR_CONTROL_HOST` **and** `TOR_CONTROL_PORT` are defined, TCP is preferred.

IPC is not supported on Windows.

## `TOR_CONTROL_HOST` and `TOR_CONTROL_PORT`

Make Tor Browser use a TCP connection to the control port. The default is `127.0.0.1:9151`

Equivalent preferences: `extensions.torlauncher.control_host`, `extensions.torlauncher.control_port`.

The host is always ignored when the browser launches the tor daemon, which will always listen only on `localhost` instead.

## `TOR_CONFIGURE_ONLY`

Normally, when Tor Browser launches the tor daemon, it also sends `TAKEOWNERSHIP` on the control port.
As a result, `tor` exits when the connection to the control port on which this has been sent is closed (which should happen only when the browser is closed).

This option prevents Tor Browser from sending `TAKEOWNERSHIP`.

The browser will never do it anyway when an external tor daemon is used.

Equivalent preference: `extensions.torlauncher.only_configure_tor`.

# SOCKS settings

For SOCKS settings, most of the control port considerations apply.

## `TOR_TRANSPROXY`

Tells Tor Browser not to use a proxy, because the network is already going through tor in another way (tor-over-tor is not good).

**Notice: this variable is active whenever it exists, its content is never checked. So, `TOR_TRANSPROXY=0` still activates the transparent proxy functionality, you need to delete the variable if you do not want to use it.**

## `TOR_SOCKS_IPC_PATH`

Makes `tor` listen on a Unix socket for SOCKS connections.

The equivalent preference: `extensions.torlauncher.socks_ipc_path`.

You can enable IPC without specifying a path with the related preference `extensions.torlauncher.socks_port_use_ipc`.

IPC is not supported on Windows.

## `TOR_SOCKS_HOST` and `TOR_SOCKS_PORT`

Make `tor` listen on a TCP socket for SOCKS connections.

If not specified, Tor Browser will ask the tor daemon to match `network.proxy.socks` and `network.proxy.socks_port`, which are Firefox's preferences to set a SOCKS proxy.

The default host is `127.0.0.1` and port is `9150`.

Like the control port, when Tor Browser starts a `tor` instance, the host will be ignored and the daemon will listen only on `localhost`.

## `extensions.torlauncher.socks_port_flags`

Additional arguments for the SOCKS port.
They will be passed directly to the tor daemon command line, so refer to `tor`'s man page.

# Other settings

## `TOR_SKIP_LAUNCH`

It prevents Tor Browser from launching a `tor` process.

Equivalent preference: `extensions.torlauncher.start_tor` (but with opposite logic: by default it is `true`, and the browser will launch `tor`).

`MOZ_BROWSER_TOOLBOX_PORT` (used for some testing automation) also implies this option.

(`TOR_SKIP_CONTROLPORTTEST` is active whenever it exists, its content is never checked).

Skipping the launch will also disable `about:preferences#connection` and the browser is expected to never call `SETCONF`.

## File paths

The following paths can be customized with a preference looking like `extensions.torlauncher.${type}_path`:

- `tor`: the path to the `tor` binary (`tor.exe` on Windows)
- `torrc`: the path of the tor configuration file
- `tordatadir`: the directory where `tor` will store its data (`DataDirectory` argument of `tor`). It should have `700` permissions
- `toronionauthdir`: the directory containing v3 hidden service authorization files (`ClientOnionAuthDir` argument of `tor`). It should have `700` permissions
- `pt-startup-dir`: will be used as a working directory for the `tor` process, and pluggable transport binaries' paths should be relative to this directory

In addition to that, `$XDG_RUNTIME_DIR` (e.g., `/run/user/$UID` on Debian) will be used as a default directory to create Unix sockets in.

## Domain isolator

The domain isolator is the component that associates SOCKS credentials to first party domains and allows to request new circuits.

The other effect of the legacy `extensions.torbutton.use_nontor_proxy` is to disable isolation.

This is useful for services that have different first-party domain but for some reason expects requests to come from the same IP address.

## `TOR_DEFAULT_HOMEPAGE`

It allows customizing `browser.startup.homepage`, unless this preference has already been customized.
Used by some downstream distributors.

# Unsupported/legacy options, variables and preferences

## Tor Check Service

**Removed in 13.0.**

`about:tor` used to become red if it detected something wrong with tor.

It has two mutually exclusive ways of doing so:

- _local test_ (default): it asked tor how to reach its SOCKS interface over the control port, and then checked that the browser configuration included at least one
- _remote test_: it fetched `https://check.torproject.org/?TorButton=true` (customizable with `extensions.torbutton.test_url`, but the check will work only if the new URL follows the same structure), and the outcome of the test depended on the downloaded content

`TOR_SKIP_CONTROLPORTTEST` forces the remote test.
Another way was setting `extensions.torbutton.local_tor_check` to `false`.

Finally, the tor test could be completely disabled with `extensions.torbutton.test_enabled` (set to `false`) and was disabled also with the (probably legacy) preference `extensions.torbutton.use_nontor_proxy` set to `true`.

## `TOR_FORCE_NET_CONFIG`

Legacy option that forced the browser to start the tor daemon with network disabled.
This option is read but ignored.

The default now is to always start tor with the network disabled and show the `about:torconnect` captive portal.
The bootstrap will be done automatically only if quickstart is enabled (`torbrowser.settings.quickstart.enabled`, but also exposed in `about:torconenct` itself and in `about:preferences#connection`).

Previously `TOR_FORCE_NET_CONFIG` was an alias for `extensions.torlauncher.prompt_at_startup`.
This preference is still used, but **it is changed automatically** by the browser whenever a bootstrap succeeds or fails (to prevent quickstart from running again if the browser is restarted after a bootstrap failed).

## Log levels

`extensions.torlauncher.loglevel` and `extensions.torlauncher.logmethod` set the log preferences for Tor Launcher.
They are not followed anymore.

The related torbutton preferences (`extensions.torbutton.loglevel`, `extensions.torbutton.logmethod`) are still valid, but they will be removed, eventually.

## `extensions.torlauncher.prompt_for_locale`

Tor Browser will try to match the OS locale, if supported, or fallback to English.

At the first start, users will be shown a button to go changing the language in the notification bar.

Then it will always be possible to change language in `about:preferences`.

## Development settings

In addition to everything listed in this page, we have some preferences that are intended only for development purposes.
They are not guaranteed to work forever.